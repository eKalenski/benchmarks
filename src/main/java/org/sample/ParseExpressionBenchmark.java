package org.sample;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class ParseExpressionBenchmark {


    private ExpressionParser PARSER;
    private String conditionExpressionString;
    private Map<String, Expression> map;

    @Setup
    public void prepare() {
        PARSER = new SpelExpressionParser();
        conditionExpressionString = "postCode.length().equals(6) && postCode.matches('[0-9]+')";
        map = new HashMap<>();
        map.put(conditionExpressionString, PARSER.parseExpression(conditionExpressionString));
    }

    @Benchmark
    public Expression testParseExpression() {
        return PARSER.parseExpression(conditionExpressionString);
    }

    @Benchmark
    public Expression testParseExpressionFromMap() {
        return map.computeIfAbsent(conditionExpressionString, key -> PARSER.parseExpression(key));
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
            .include(ParseExpressionBenchmark.class.getSimpleName())
            .warmupIterations(5)
            .measurementIterations(5)
            .warmupTime(TimeValue.seconds(1))
            .measurementTime(TimeValue.seconds(1))
            .forks(1)
            .build();

        new Runner(opt).run();
    }

}
